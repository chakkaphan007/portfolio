﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class OptionScript : MonoBehaviour
{
    public AudioMixer mixer;
    public void SetLevel (float SliderValue)
    {
        mixer.SetFloat("MusicVol",Mathf.Log10(SliderValue)*20);
    }
}
