﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartSceneScript : MonoBehaviour
{
    
    private float waitTime;
    public float startWaitTime = 5;
    public static bool canClick = false;
    [SerializeField] Text _Click;

    //Camera
    Animator anim;
    public string currentState;
    const string CAMFIRST = "CameraCover";
    const string CAMSTART = "CameraCover2";
    const string CAMOPTIONGO = "CameraCover4";
    const string CAMOPTIONBACK = "CameraCover5";
    const string CAMSTAGE = "CameraCover3";

    void Start()
    {
        
        waitTime = startWaitTime;
        _Click.gameObject.SetActive(false);
        anim = GetComponent<Animator>();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
    }
    void Update()
    {
        if (waitTime <= 0 && canClick == false)
        {
            _Click.gameObject.SetActive(true);
            canClick = true;
        }
        if (canClick && Input.anyKeyDown)
        {
            _Click.gameObject.SetActive(false);
            anim.SetTrigger("COVER");
        }
        waitTime -= Time.deltaTime;
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "Option")
                {
                    anim.SetTrigger("OPTIONGO");
                }
                else if (hit.transform.name == "BackOption")
                {
                    anim.SetTrigger("OPTIONBACK");
                }
                else if (hit.transform.name == "Door")
                {
                    anim.SetTrigger("START");
                    Invoke("StartGame",1.5f);
                    /*if (GameInstance.Instance.IsTutorial == false)
                    {
                        Invoke("startButt",1.5f);
                        GameInstance.Instance.IsTutorial = true;
                    }
                    else if(GameInstance.Instance.IsTutorial == true)
                    {
                        Invoke("Stagebutt",1.5f);
                    }*/
                }
                else if (hit.transform.name == "EXit")
                {
                    
                    Application.Quit();
                }
                
            }
        }
    }
            public void StartButtonClick(Button button)
            {
                if (GameInstance.Instance.IsTutorial == false)
                {
                    anim.SetTrigger("START");
                    GameInstance.Instance.IsTutorial = true;
                }
                else if (GameInstance.Instance.IsTutorial == true)
                {
                    anim.SetTrigger("START");
                }
            }
            private void startButt()
            {
                 SceneManager.LoadScene("Tutorials");
            }
            private void Stagebutt()
            {
                SceneManager.LoadScene("StageSelect");
            }
            private void StartGame(){
                SceneManager.LoadScene("JapanScene");
            }
        }