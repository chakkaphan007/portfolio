﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestPatrol : MonoBehaviour
{
    [SerializeField] 
    public Text _taskList;
    public Text _happy;
    public Text _gel;
    public GameObject _maskIcon;
    public GameObject _gelIcon;
    public Image _happypink;
    public Image _gelblue;

    
    private float happinessMax = 100;
    private float happiness;
    private float gelMax = 8;
    private float gel;
    void Start()
    {
        happiness = 100;
        gel = 8;
        //randomSpot = 0;
    }

    // Update is called once per frame
    void Update()
    {
        _taskList.text = "Today Task \n - Give Mask : " + GameInstance.Instance.Task_GiveMask +"/3 \n - Refill Gel : " + GameInstance.Instance.Task_RefillGel
        + "/1 \n - Collect Fruit : " + GameInstance.Instance.Task_CollectFruit + "/5";
        
        //Send To GameInstance
        GameInstance.Instance.R_Happiness = happiness;

        if(happiness > 100) {happiness = happinessMax;}
        if(gel > 8) {gel = gelMax;}
        if(gel < 0) {gel = 0;}
        //Score and Happiness Display
        _happypink.fillAmount = happiness / 100;
        _gelblue.fillAmount = gel / 8;
        _happy.text = "Happiness : " + System.Math.Round(happiness,0).ToString() + " / 100";
        _gel.text = "Gel : " + gel.ToString() + " / 8";
        if(gel <= 0){
            _gelIcon.SetActive(true);
        }
        else _gelIcon.SetActive(false);
        //FruitDrop
        if(GameInstance.Instance.IsFruit == true){
            happiness -= 0.2f * Time.deltaTime;
        }
        //NoMaskInShop Happydown
        if(GameInstance.Instance.IsnoMask == true){
            happiness -= 0.5f * Time.deltaTime;
            _maskIcon.SetActive(true);
        }
        else _maskIcon.SetActive(false);

    }
    public void IsNoMaskIn(){
        GameInstance.Instance.IsnoMask = true;
    }
    public void IsNoMaskOut(){
        GameInstance.Instance.IsnoMask = false;
    }

    public void UseGel()
    {
        if(gel == 0) happiness -= 5;
        gel--;
    }
    public void RefillGel()
    {
        gel += 8;
        GameInstance.Instance.Task_RefillGel++;
    }
    public void FruitHappy()
    {
        happiness +=5;
    }
    public void CheckBillHapDown()
    {
        happiness -=5;
    }
}
