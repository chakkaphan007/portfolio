﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBot : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Bot" || other.gameObject.tag == "BotnoMask"||other.gameObject.tag == "BotLast"){
            Destroy(other.gameObject);
        }
    }
}
