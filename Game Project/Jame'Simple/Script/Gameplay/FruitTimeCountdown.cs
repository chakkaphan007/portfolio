﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FruitTimeCountdown : MonoBehaviour
{
        [SerializeField] 
        public Text _fruitTimeText;
        public float destroyFruitTime;
        public float Count;
        void start(){
            Count = destroyFruitTime;
        }
        void update(){
            _fruitTimeText.text = "Time : " + System.Math.Round(Count,2).ToString();
            if(Count <= 0){
               
                }
                else{
                    Count -= Time.deltaTime;
                }
        }
}
