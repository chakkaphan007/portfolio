﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MiniGameSpawner : MonoBehaviour
{
    public GameObject _fruitIcon;
    public Image _fruitImage;
    public GameObject[] _spawnObject;
    public Sprite AppleImage; 
    public Sprite BreadImage; 
    public Sprite BananaImage; 
    public Sprite PeachImage; 
    public Sprite PearImage;
    private int rand;
    private int randSpawn;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("MiniGameSpawn", 5);
    }

    void MiniGameSpawn()
    {
        rand = Random.Range(0,5);

        /*randSpawn = Random.Range(0,10);*/

        //Debug.Log("Rand = " + randSpawn);
        if(rand == 0){
            _fruitImage.sprite = AppleImage;
        }
        else if(rand == 1){
            _fruitImage.sprite = BreadImage;
        }
        else if(rand == 2){
            _fruitImage.sprite = BananaImage;
        }
        else if(rand == 3){
            _fruitImage.sprite = PeachImage;
        }
        else if(rand == 4){
            _fruitImage.sprite = PearImage;
        }

        _fruitIcon.SetActive(true);
        _spawnObject[rand].transform.position = new Vector3(Random.Range(5.5f, 12.5f), 3.5f, 
            Random.Range(12f, 19f));
        /*if(randSpawn < 5){
            
        }
        else{
            _spawnObject[rand].transform.position = new Vector3(Random.Range(-12f, -10f), 3.5f, 
            Random.Range(-5.5f, 12.5f));
        }*/

        _spawnObject[rand].SetActive(true);
        GameInstance.Instance.IsFruit= true;
        float DesTime = 15;
        float nextSpawnTime = 16 /*Random.Range(10, 15)*/;

        Invoke("MiniGameSpawn", nextSpawnTime);
        Invoke("fruitOff", DesTime);
    }
    void fruitOff(){
        GameInstance.Instance.IsFruit= false;
        _fruitIcon.SetActive(false);
        _spawnObject[rand].SetActive(false);
    }
}
