﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JelRefill : MonoBehaviour
{    
    public TestPatrol _TestPatrol;
    void OnTriggerStay(Collider other)
    {
        if(GameInstance.Instance.IsPlayerGotJel == true){
            if (other.gameObject.tag == "Player" && Input.GetKeyDown(KeyCode.E))
            {
                _TestPatrol.RefillGel();
            }
        }
    }
}

