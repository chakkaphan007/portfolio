﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    // Start is called before the first frame update
    public void RespawnMaskToCounter(){
        this.gameObject.transform.position = new Vector3(1.55f,1.67f,9.25f);
        transform.rotation = Quaternion.identity;
    }

    public void RespawnJelToCounter(){
        this.gameObject.transform.position = new Vector3(1.38f,1.52f,7.95f);
        this.gameObject.transform.rotation = Quaternion.identity;
    }
}
