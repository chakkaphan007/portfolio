﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotNoMaskText : MonoBehaviour
{
    public GameObject _GivemaskText;
    // Start is called before the first frame update
    public void TextAppear(){
        if(this.gameObject.tag == "BotnoMask"){
            _GivemaskText.SetActive(true);
        }
    }
    public void TextDisAppear(){
            _GivemaskText.SetActive(false);
    }
}
