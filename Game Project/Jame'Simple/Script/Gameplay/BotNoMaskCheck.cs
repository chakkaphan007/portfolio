﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotNoMaskCheck : MonoBehaviour
{
    void OnTriggerStay(Collider other){
        if(other.gameObject.tag == "BotnoMask"){
            GameInstance.Instance.IsnoMask = true;
        }
        //else GameInstance.Instance.IsnoMask = false;
    }
    void OnTriggerExit(Collider other){
        if(other.gameObject.tag == "BotnoMask"){
            GameInstance.Instance.IsnoMask = false;
        }
    }
}
