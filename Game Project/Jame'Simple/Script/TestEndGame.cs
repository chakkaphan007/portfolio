﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestEndGame : MonoBehaviour
{
  public void SkipToEnd()
  {
      SceneManager.LoadScene("ScoreCheck");
  }
}
