﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultControl : MonoBehaviour
{
    public Text ScoreText;
    public float _score = 1;

    void Start()
    {
        _score = GameInstance.Instance.R_Happiness * 100;
        ScoreText.text = "Total Score : " + _score.ToString(); 
        GameInstance.Instance.Task_GiveMask = 0;
        GameInstance.Instance.Task_RefillGel = 0;
        GameInstance.Instance.Task_CollectFruit = 0;
        GameInstance.Instance.IsnoMask = false;
        GameInstance.Instance.IsFruit = false;
        GameInstance.Instance.IsPlayerGotMask = false;
        GameInstance.Instance.IsPlayerGotJel = false;
        GameInstance.Instance.IsPlayerCollectFruit=false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void BackToMainMenu(){

        SceneManager.LoadScene("Cover");

    }
    public void PlayAgain(){

        SceneManager.LoadScene("JapanScene");

    }
}
