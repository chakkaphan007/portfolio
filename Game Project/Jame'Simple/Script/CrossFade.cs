﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CrossFade : MonoBehaviour {
    public Animator anim;
    public float transition = 2f;
    // Start is called before the first frame updat

    // Update is called once per frame
    void Update () {

    }
    public void LoadNextLevel () {
        StartCoroutine (LoadLevel (SceneManager.GetActiveScene ().buildIndex + 1));
    }
    IEnumerator LoadLevel (int LevelIndex) {
        anim.SetTrigger("Crossfade");
        yield return new WaitForSeconds (transition);
        SceneManager.LoadScene (LevelIndex);
    }
}