﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppleTriggerFruitTask : MonoBehaviour
{
    public TestPatrol _TestP;
    public GameObject _fruitIcon;
    void OnTriggerEnter(Collider collider)
    {
       
        if(collider.gameObject.tag=="AppleBox"){
            
            
            //Destroy(GameObject.Find("vActionText_ExampleA (4)"));
            //Destroy(gameObject,2);
            _TestP.FruitHappy();
            _fruitIcon.SetActive(false);
            GameInstance.Instance.Task_CollectFruit++;
            gameObject.SetActive(false);
            
            
            
        }
      
         
        
    }
      
   
    
    
}
