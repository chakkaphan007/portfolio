﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeachTriggerFruitTask : MonoBehaviour
{
    public GameObject _PickText;
    public TestPatrol _TestP;
    public GameObject _fruitIcon;
    void OnTriggerEnter(Collider collider)
    {
       
        if(collider.gameObject.tag=="PeachBox"){
            
            
            //Destroy(GameObject.Find("vActionText_ExampleA (4)"));
          
 
            //Destroy(gameObject,2);
            _PickText.SetActive(false);
            _TestP.FruitHappy();
             _fruitIcon.SetActive(false);
             GameInstance.Instance.IsFruit = false;
            GameInstance.Instance.Task_CollectFruit++;
            gameObject.SetActive(false);
         
            
            
        }
      
         
        
    }
      
   
    
    
}
