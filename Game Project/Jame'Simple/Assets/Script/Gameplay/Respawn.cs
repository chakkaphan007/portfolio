﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Respawn : MonoBehaviour
{   void Awake(){
      Scene scene = SceneManager.GetActiveScene();
 
      if (scene.name == "Cover")
      {
            Destroy(this.gameObject);
            Debug.Log("Destroy Mask Jel");
      }
}
    void Update()
    {
        if(GameInstance.Instance.IsMenu == true){
            Destroy(this.gameObject);
        }
    }
    public void RespawnMaskToCounter(){
        this.gameObject.transform.position = new Vector3(1.55f,1.67f,9.25f);
        transform.rotation = Quaternion.identity;
    }

    public void RespawnJelToCounter(){
        this.gameObject.transform.position = new Vector3(1.38f,1.52f,7.95f);
        this.gameObject.transform.rotation = Quaternion.identity;
    }
}
