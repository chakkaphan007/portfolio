﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JelRefill : MonoBehaviour
{   public GameObject _TextRefill;
    public TestPatrol _TestPatrol;
    void OnTriggerStay(Collider other)
    {
        if(GameInstance.Instance.IsPlayerGotJel == true){
            _TextRefill.SetActive(true);
            if (other.gameObject.tag == "Player" && Input.GetKeyDown(KeyCode.E))
            {
                _TestPatrol.RefillGel();
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        _TextRefill.SetActive(false);
    }
}

