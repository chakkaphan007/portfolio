﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultControl : MonoBehaviour
{
    public GameObject[] _star;
    public Text ScoreText;
    public float _score = 1;

    void Start()
    {
        GameInstance.Instance.IsMenu = true;
        _score = GameInstance.Instance.R_Happiness * 100;
        if(GameInstance.Instance.IsLose != true){
            ScoreText.text = "Total Score : " + System.Math.Round(_score,0).ToString();
            if(_score < 8000){
                _star[4].SetActive(false);
            if(_score < 6000){
                _star[3].SetActive(false);}
            if(_score < 4000){
                _star[2].SetActive(false);}
            if(_score < 2000){
                _star[1].SetActive(false);}
            }
        }
        else {ScoreText.text = "                   You Lose";
                _star[4].SetActive(false);
                _star[3].SetActive(false);
                _star[2].SetActive(false);
                _star[1].SetActive(false);
                _star[0].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void BackToMainMenu(){
        SceneManager.UnloadScene("JapanScene");
        SceneManager.LoadScene("Cover");
    }
    public void PlayAgain(){
        SceneManager.UnloadScene("JapanScene");
        this.gameObject.SetActive(false);
        SceneManager.LoadScene("JapanScene");
        GameInstance.Instance.IsMenu = false;
    }
}
