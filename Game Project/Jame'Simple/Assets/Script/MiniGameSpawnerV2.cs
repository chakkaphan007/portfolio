﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameSpawnerV2 : MonoBehaviour {
    public GameObject[] ObjectsToSpawn;
    public Transform[] SpawnPoints;
    public float Respawntime = 5;

    void Start () {
        StartCoroutine (SpawningOrder ());
    }

    public IEnumerator SpawningOrder () {
        while (true) {
            yield return new WaitForSeconds (Respawntime);
            GameObject Spawned = Instantiate (ObjectsToSpawn[Random.Range (0, ObjectsToSpawn.Length)], SpawnPoints[Random.Range (0, SpawnPoints.Length)].position, Quaternion.identity);
            Spawned.SetActive(true);
            Debug.Log ("Spawning" + Spawned.name);

        }
    }
}