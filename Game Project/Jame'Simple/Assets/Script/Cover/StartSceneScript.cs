﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;    

public class StartSceneScript : MonoBehaviour
{
    public TextMeshPro VolumeText;
    public AudioSource source;
    private float waitTime;
    public float startWaitTime = 5;
    public static bool canClick = false;
    [SerializeField] Text _Click;

    //Camera
    Animator anim;
    public string currentState;
    const string CAMFIRST = "CameraCover";
    const string CAMSTART = "CameraCover2";
    const string CAMOPTIONGO = "CameraCover4";
    const string CAMOPTIONBACK = "CameraCover5";
    const string CAMSTAGE = "CameraCover3";
    private float VolumeCount;

     void Awake()
    {
        
    }
    void Start()
    {
        source.volume = 0;
        waitTime = startWaitTime;
        _Click.gameObject.SetActive(false);
        anim = GetComponent<Animator>();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
    }
    void Update()
    {   
        if(Input.GetKey(KeyCode.F5))
        {
            SceneManager.LoadScene("JapanScene");
        }
        GameInstance.Instance.SoundMusic = VolumeCount;
        if(VolumeCount >= 1)
        {
            VolumeCount = 1;
        }
        else if (VolumeCount <= 0)
        {
            VolumeCount = 0;
        }
        float Vtext = VolumeCount * 100;
        VolumeText.text = System.Math.Round(Vtext,0).ToString();


        if (waitTime <= 0 && canClick == false)
        {
            _Click.gameObject.SetActive(true);
            canClick = true;
        }
        if (canClick && Input.anyKeyDown)
        {
            _Click.gameObject.SetActive(false);
            anim.SetTrigger("COVER");
        }
        waitTime -= Time.deltaTime;
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "OptionButton")
                {
                    anim.SetTrigger("OPTIONGO");
                }
                else if (hit.transform.name == "Back")
                {
                    anim.SetTrigger("OPTIONBACK");
                }
                else if (hit.transform.name == "plus")
                {
                    StartCoroutine(Volumeplus());
                    Debug.Log(source.volume);
                }
                else if (hit.transform.name == "minus")
                {
                    StartCoroutine(Volumeminus());
                    Debug.Log(source.volume);
                }
                else if (hit.transform.name == "Door")
                {
                    anim.SetTrigger("START");
                if (GameInstance.Instance.IsTutorial == false)
                {
                    Invoke("startButt",1.5f);
                    GameInstance.Instance.IsTutorial = true;
                }
                else if(GameInstance.Instance.IsTutorial == true)
                {
                    Invoke("Stagebutt",1.5f);
                }
                else if (hit.transform.name == "Exit")
                {
                    Application.Quit();
                }
            }
        }
        }
    }
            public void StartButtonClick(Button button)
            {
                if (GameInstance.Instance.IsTutorial == false)
                {
                    anim.SetTrigger("START");
                    GameInstance.Instance.IsTutorial = true;
                }
                else if (GameInstance.Instance.IsTutorial == true)
                {
                    anim.SetTrigger("START");
                }
            }
            private void startButt()
            {
                 SceneManager.LoadScene("JapanScene");
            }
            
            public IEnumerator Volumeplus()
            {
                    VolumeCount = VolumeCount + 0.1f;
                    yield return null;
                    source.volume = VolumeCount;          
            }
            public IEnumerator Volumeminus()
            {
                    VolumeCount = VolumeCount - 0.1f;
                    yield return null;
                    source.volume = VolumeCount;
                    
            }
        }