﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallSpawner : MonoBehaviour {
    private Transform PlayerTransform;
    private Transform OutwallTransform;
    private float spawnY = 20.0f;
    private float OutwallspawnY = 20.0f;
    private float OutwallspawnY2 = 20.0f;
    private float OrbspawnY = 0.0f;
    private float OrbspawnY2 = 0.0f;


    private float TotemLength = 15f;
    private float OutwallLength = 20f;
    private float OutwallLength2 = 20f;
    private float OrbLength = 50f;


    private int amnOnScreen = 5;
    public GameObject[] Wall;
    public GameObject[] OutWall;
    public GameObject[] Orb;
    private float minX = -20;
    private float maxX = -8;
    private float minX2 = 8;
    private float maxX2 = 20;
    private float OrbminX = -18;
    private float OrbmaxX = -5;
    private float OrbminX2 = 5;
    private float OrbmaxX2 = 18;

    public static bool StartGame = false;
    void Awake()
    {

        PlayerTransform = GameObject.FindGameObjectWithTag("Player").transform;
            if(StartGame == true)
        {
        }
    }   
    void Update()
    {
        
        if (PlayerTransform.position.y > (spawnY - amnOnScreen * TotemLength)&&StartGame == true)
        {
            spawnTotem();
            spawnTotem2();
            spawnTotem();
            spawnTotem2();
        }
        if (PlayerTransform.position.y > (OrbspawnY - amnOnScreen * OrbLength)&&StartGame == true)
        {
            spawnOrb();
            spawnOrb();
        }
        if (PlayerTransform.position.y > (OutwallspawnY - amnOnScreen * OutwallLength)&&StartGame == true)
        {
            spawnOutWall();
            spawnOutWall();
            spawnOutWall();
            spawnOutWall();
        }
        if (PlayerTransform.position.y > (OutwallspawnY2 - amnOnScreen * OutwallLength2)&&StartGame == true)
        {
            spawnOutWall2();
            spawnOutWall2();
            spawnOutWall2();
            spawnOutWall2();
        }
    } 
    private void spawnTotem(int prefabIndex = -1)
    {
        Vector3 spawnPosition = new Vector3();
        spawnPosition.x += Random.Range(minX,maxX);
        GameObject Totem;
        Totem = Instantiate(Wall[0],spawnPosition, Quaternion.identity);
        Totem.transform.SetParent(transform);
        Totem.transform.position = (Vector3.up  * spawnY)+ spawnPosition;
        spawnY += TotemLength;
    }
        private void spawnTotem2(int prefabIndex = -1)
    {
        Vector3 spawnPosition = new Vector3();
        spawnPosition.x += Random.Range(minX2,maxX2);
        GameObject Totem;
        Totem = Instantiate(Wall[0],spawnPosition, Quaternion.identity);
        Totem.transform.SetParent(transform);
        Totem.transform.position = (Vector3.up  * spawnY)+ spawnPosition;
        spawnY += TotemLength;
    }
        private void spawnOrb(int prefabIndex = -1)
    {
        Vector3 spawnPosition = new Vector3();
        spawnPosition.x += Random.Range(OrbminX,OrbmaxX);
        GameObject Totem;
        Totem = Instantiate(Orb[0],spawnPosition, Quaternion.identity);
        Totem.transform.SetParent(transform);
        Totem.transform.position = (Vector3.up  * OrbspawnY)+ spawnPosition;
        OrbspawnY += OrbLength;
    }
            private void spawnOrb2(int prefabIndex = -1)
    {
        Vector3 spawnPosition = new Vector3();
        spawnPosition.x += Random.Range(OrbminX2,OrbmaxX2);
        GameObject Totem;
        Totem = Instantiate(Orb[1],spawnPosition, Quaternion.identity);
        Totem.transform.SetParent(transform);
        Totem.transform.position = (Vector3.up  * OrbspawnY)+ spawnPosition;
        OrbspawnY2 += OrbLength;
    }
        private void spawnOutWall(int prefabIndex = -1)
    {
        Vector3 spawnPosition = new Vector3();
        spawnPosition.x += Random.Range(-25.9f,-25.9f);
        GameObject Totem;
        Totem = Instantiate(OutWall[0],spawnPosition, Quaternion.identity);
        Totem.transform.SetParent(transform);
        Totem.transform.position = (Vector3.up  * OutwallspawnY)+ spawnPosition;
        OutwallspawnY += OutwallLength;
    }
        private void spawnOutWall2(int prefabIndex = -1)
    {
        Vector3 spawnPosition = new Vector3();
        spawnPosition.x += Random.Range(29.1f,29.1f);
        GameObject Totem;
        Totem = Instantiate(OutWall[1],spawnPosition, Quaternion.identity);
        Totem.transform.SetParent(transform);
        Totem.transform.position = (Vector3.up  * OutwallspawnY2)+ spawnPosition;
        OutwallspawnY2 += OutwallLength2;
    }
    IEnumerator RandomOrb()
    {
        while(true)
        {
            spawnOrb();
            yield return new WaitForSeconds(2f);
        }
        
    }
}
