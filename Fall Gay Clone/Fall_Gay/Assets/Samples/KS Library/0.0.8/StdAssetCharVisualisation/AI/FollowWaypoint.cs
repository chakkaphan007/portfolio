﻿using System.Security.Cryptography;
using KS.AI.FSM;
using KS.Character;
using UnityEngine;

namespace StdAssetCharVisualisation.AI
{
    public class FollowWaypoint : State
    {
        public FSMFollowWaypoints LocalFSM { get; set; }
        
        private int currentWaypointIdx = -1;

        private UnityEngine.AI.NavMeshAgent agent;
        private ThirdPersonCharacterKSModified thirdPersonCharacterKsModified;
        private AICharacterControlKSModified aiCharacterControlKsModified;
        
        public FollowWaypoint(FiniteStateMachine fsm) : base(fsm)
        {
            LocalFSM = (FSMFollowWaypoints)fsm;
            
            agent = ((FSMFollowWaypoints) this.FSM).Agent;
            thirdPersonCharacterKsModified = ((FSMFollowWaypoints) this.FSM).ThirdPersonChar;
            aiCharacterControlKsModified = ((FSMFollowWaypoints) this.FSM).AICharControlKsModified;
            
            agent.speed = Random.Range(LocalFSM.aiSettings.minMoveSpeed, LocalFSM.aiSettings.maxMoveSpeed);
        }

        public override void Enter()
        {
            float lastDist = Mathf.Infinity; // Store distance between NPC and waypoints.
            
            // Calculate closest waypoint by looping around each one and calculating the distance between the NPC and each waypoint.
            for (int i = 0; i < LocalFSM.aiSettings.waypoints.Count; i++)
            {
                GameObject thisWP = LocalFSM.aiSettings.waypoints[i].gameObject;
                float distance = Vector3.Distance(LocalFSM.Npc.transform.position, thisWP.transform.position);
                if(distance < lastDist)
                {
                    currentWaypointIdx = i;
                    lastDist = distance;

                    LocalFSM.AICharControlKsModified.SetTarget(thisWP.transform);
                }            
            }
            
            base.Enter();
        }

        public override void Update()
        {
            if (aiCharacterControlKsModified.target != null)
                agent.SetDestination(aiCharacterControlKsModified.target.position);

            if (agent.remainingDistance > agent.stoppingDistance)
            {
                //Move the agent
                thirdPersonCharacterKsModified.Move(agent.desiredVelocity, false, false);
            }
            else
            {
                if (Random.Range(0.0f, 100.0f) <  LocalFSM.aiSettings.chanceOfIdle)
                {
                    //increase waypoint index
                    if (currentWaypointIdx >= LocalFSM.aiSettings.waypoints.Count - 1)
                    {
                        currentWaypointIdx = 0;
                    }
                    else
                    {
                        currentWaypointIdx++;
                    }
                }
                else
                {
                    this.FSM.NextState = new Idle(this.FSM);
                    this.StateStage = StateEvent.EXIT;
                }

                //Set target to the next waypoint
                aiCharacterControlKsModified.SetTarget(LocalFSM.aiSettings.waypoints[currentWaypointIdx]);
                agent.SetDestination(aiCharacterControlKsModified.target.position);

                //Stop the character movement
                thirdPersonCharacterKsModified.Move(Vector3.zero, false, false);
            }
        }
        
        public override void Exit()
        {
            base.Exit();
        }
    }
}
