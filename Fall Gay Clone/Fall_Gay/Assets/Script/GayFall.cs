﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GayFall : MonoBehaviour
{
    public GameObject Block;
   Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other) 
    {
        if(other.tag == "Player")
        {
            Debug.Log("Touch");
            StartCoroutine(delete());
        }
    }
    public IEnumerator delete()
    {
        anim.SetTrigger("Block");
        yield return new WaitForSeconds(2f);
        Destroy(Block);
    }
}
