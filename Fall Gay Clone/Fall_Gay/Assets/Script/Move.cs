﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {
    public float torque = 1000f;
    Rigidbody rb;
    void Start () {
        rb = GetComponent<Rigidbody> ();
    }
    void Update () {
        float H = Input.GetAxis ("Horizontal");
        float V = Input.GetAxis ("Vertical");
        Vector3 move = new Vector3 (H, 0.0f, V);
        if(Input.anyKey){
        rb.AddForce (move * torque * Time.deltaTime);
        }
        else
        { 
            rb.AddForce (move * torque * 0);
        }
    }
}
