﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Run : MonoBehaviour {
    public GameObject Object;
    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        if (Key.runcount == 1) {
            Key.missioncount = 0;
            Object.GetComponent<Text> ().text = "Run and find the Key!!";
        }
        if (Key.runcount == 0) {
            Object.GetComponent<Text> ().text = "";
        }
    }
}