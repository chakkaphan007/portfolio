﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DoorText : MonoBehaviour {
    public GameObject Object;
    // Start is called before the first frame update
    void Start () { }

    // Update is called once per frame
    void Update () {
        if (DoorDetec.Doorcount >= 1) {
            Debug.Log ("go");
            Object.GetComponent<Text> ().text = "You Don't have a key!";
        }
        if (DoorDetec.Doorcount == 0) {
            Object.GetComponent<Text> ().text = "";
        }
        if (DoorDetec.Doorcount >= 1 && Key.Keycount >= 1) {
            Debug.Log ("hh");
            SceneManager.LoadScene ("Win");
        }
    }
}