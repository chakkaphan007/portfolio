﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCam : MonoBehaviour
{
    private Camera cam;
    public VariableJoystick variableJoystick;
    public float mouseSen = 100f;
    float xRotation = 0f;
    public Transform player;

    // Update is called once per frame
    void Update()
    {
        float mouseX = variableJoystick.Horizontal * mouseSen * Time.deltaTime;
        float mouseY = variableJoystick.Vertical * mouseSen * Time.deltaTime;
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -20f, 20f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        player.Rotate (Vector3.up * mouseX);
    }
}
