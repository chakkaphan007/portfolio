﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyText : MonoBehaviour {
    GameObject Object;
    // Start is called before the first frame update
    void Start () {
        Object = GameObject.Find ("ObjNum");
    }

    // Update is called once per frame
    void Update () {
        if (Key.Keycount == 1) {
            Key.missioncount = 0;
            Key.runcount = 0;
            Object.GetComponent<Text> ().text = "You got the key! \n Find the Door";
        }
    }
}