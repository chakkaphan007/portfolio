﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorDetec : MonoBehaviour {
    public static int Doorcount = 0;

    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () { }
    void OnTriggerEnter (Collider other) {
        if (other.gameObject.tag == "Door") {
            Debug.Log ("Door");
            Doorcount++;
            Key.missioncount = 0;
            Key.runcount = 0;
        }
    }
    void OnTriggerExit (Collider other) {
        if (other.gameObject.tag == "Door") {
            Debug.Log ("exit");
            Doorcount = 0;
        }
    }
}