﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemychase : MonoBehaviour {
    public Transform Player;
    float MobdistanceRun = 3.0f;
    float outofview = 30f;
    private NavMeshAgent Mop;
    void Start () {
        Mop = GetComponent<NavMeshAgent> ();

    }
    void Update () {

        float distance = Vector3.Distance (transform.position, Player.transform.position);
        Animator Manani = GetComponentInChildren<Animator> ();
        if (distance > MobdistanceRun && distance < outofview) {
            Mop.SetDestination (Player.transform.position);
            Manani.SetBool ("Walk", true);
        } else {
            Manani.SetBool ("Walk", false);
        }
        //อยู่ในระยะplayer
        if (distance < MobdistanceRun) {
            Manani.SetBool("punch", true);
            Vector3 dirToPlayer = transform.position - transform.position;
            Vector3 newPos = transform.position - dirToPlayer;
            Mop.SetDestination (newPos);
        } else {
            Manani.SetBool ("punch", false);
        }
        if (distance > outofview) {
            Vector3 dirToPlayer = transform.position - transform.position;
            Vector3 newPos = transform.position - dirToPlayer;
            Manani.SetBool ("Walk", true);
        }
    }
}