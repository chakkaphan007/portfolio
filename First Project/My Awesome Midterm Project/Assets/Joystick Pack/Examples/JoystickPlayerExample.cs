﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickPlayerExample : MonoBehaviour
{
    public float speed;
    public VariableJoystick variableJoystick;
    private CharacterController _controller;
    
    void Start()
    {
        _controller = GetComponent<CharacterController>();
    }
    public void FixedUpdate()
    {
        Vector3 move = transform.right * variableJoystick.Horizontal + transform.forward * variableJoystick.Vertical;
        _controller.Move(move * speed * Time.deltaTime);
    }
}